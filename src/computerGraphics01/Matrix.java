package computerGraphics01;

public class Matrix {

	public final float[][] m;
	public final int m_width;
	public final int m_height;
	
	public Matrix(int width, int height)
	{
		m = new float[height][width];
		m_height = height;
		m_width = width;
		
		for(int i=0; i < m_height; i++)
			for(int j=0; j < m_width; j++)
				m[i][j] = 0;
	}
	
	public Matrix(int width, int height, float[] vals)
	{
		m = new float[height][width];
		m_height = height;
		m_width = width;
		
		for(int i=0; i < m_height; i++)
			for(int j=0; j < m_width; j++)
				if ((i*m_width + j) >= vals.length)
					m[i][j] = 0;
				else m[i][j] = vals[i*m_width + j];
	}
	
	public boolean setValue(int i, int j, float val)
	{
		final boolean valid = (i < m_height) && (j < m_width) && (i >= 0) && (j >= 0);
		if (valid)
			m[i][j] = val;
		
		return valid;
	}
	
	public void print()
	{
		for(int i=0; i < m_height; i++)
		{
			for(int j=0; j < m_width; j++)
			{
				System.out.print(m[i][j]);
				System.out.print(" , ");
			}
			System.out.println();
		}
	}
	
	public Matrix transpose()
	{
		Matrix res = new Matrix(m_height, m_width);
		for(int i = 0; i < m_height; i++)
			for(int j = 0; j < m_width; j++)
				res.m[j][i] = m[i][j];
		return res;
	}
	
	public Matrix mult(Matrix right)
	{
		Matrix res = null;
		if(this.m_width == right.m_height)
		{
			res = new Matrix(right.m_width, this.m_height);
			for(int i = 0; i < this.m_height; i++)
				for(int j = 0; j < right.m_width; j++)
					{
						float val = 0;
						for(int k = 0; k < this.m_width; k++)
							val += this.m[i][k] * right.m[k][j];	
						res.setValue(i, j, val);
					}
		}
		return res;
	}
	
	public static Matrix identMatrix(int d)
	{
		Matrix res = new Matrix(d,d);
		res.setValue(0, 0, 1);
		res.setValue(1, 1, 1);
		res.setValue(2, 2, 1);
		return res;
	}
	
	
	public static Matrix translate2d(float a, float b) // x+a, y+b
	{
		Matrix m = new Matrix(3,3);
		m.setValue(0, 0, 1);
		m.setValue(0, 2, a);
		m.setValue(1, 1, 1);
		m.setValue(1, 2, b);
		m.setValue(2, 2, 1);
		return m;
	}
	
	public static Matrix scale2d(float scaleX, float scaleY) //x * scaleX , y * scaleY
	{
		Matrix m = new Matrix(3,3);
		m.setValue(0, 0, scaleX);
		m.setValue(1, 1, scaleY);
		m.setValue(2, 2, 1);
		return m;
	}
	
	public static Matrix rotate2d(float theta) // xcos + ysin, -xsin + ycos
	{
		Matrix m = new Matrix(3,3);
		m.setValue(0, 0, (float)Math.cos(Math.toRadians(theta)));
		m.setValue(0, 1, (float)Math.sin(Math.toRadians(theta)));
		m.setValue(1, 0, (float)-Math.sin(Math.toRadians(theta)));
		m.setValue(1, 1, (float)Math.cos(Math.toRadians(theta)));
		m.setValue(2, 2, 1);
		return m;
	}
	
	public static Matrix rotateOrigin2d(float a, float b, float theta) //(a,b) zero point, xcos + ysin, -xsin + ycos, (a,b) back to normal
	{
		Matrix m = translate2d(a, b).mult(rotate2d(theta).mult(translate2d(-a,-b)));
		return m;
	}
	
	public static Matrix scaleOrigin2d(float a, float b, float scaleX, float scaleY) //(a,b) zero point, x * scaleX , y * scaleY, (a,b) back to normal 
	{
		Matrix m = translate2d(a, b).mult(scale2d(scaleX, scaleY).mult(translate2d(-a,-b)));
		return m;
	}
	
	public static Matrix shear2d(float a, float b)//x+ ay , y + bx
	{
		Matrix m = new Matrix(3,3);
		m.setValue(0, 0, 1);
		m.setValue(0, 1, a);
		m.setValue(1, 0, b);
		m.setValue(1, 1, 1);
		m.setValue(2, 2, 1);
		return m;
	}
	
	public static Matrix shearOrigin2d(float a0, float b0, float a, float b) //(a0,b0) zero point, x+ ay , y + bx, (a0,b0) back to normal
	{
		Matrix m = translate2d(a0, b0).mult(shear2d(a, b).mult(translate2d(-a0,-b0)));
		return m;
	}
	
	
}
