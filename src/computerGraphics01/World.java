package computerGraphics01;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;



public class World {
	
	public class Edge {
		public final int v1;
		public final int v2;
		Edge(int u1, int u2)
		{
			v1 = u1;
			v2 = u2;
		}
	}
	
	public final List<Vector2d> m_VertexList;
	public final List<Edge> m_EdgeList;
	
	public World()
	{
		m_VertexList = new ArrayList<Vector2d>();
		m_EdgeList = new ArrayList<Edge>();
	}
	
	public void addVertex(Vector2d ver)
	{
		for(int i = 0; i < m_VertexList.size(); i++)
		{
			if ((m_VertexList.get(i).Vx == ver.Vx) && (m_VertexList.get(i).Vy == ver.Vy))
				return;
		}
		m_VertexList.add(ver);
	}
	
	public void addEdge(Edge e)
	{
		for(int i = 0; i < m_EdgeList.size(); i++)
		{
			if (((m_EdgeList.get(i).v1 == e.v1) && (m_EdgeList.get(i).v2 == e.v2)) 
			|| ((m_EdgeList.get(i).v1 == e.v2) && (m_EdgeList.get(i).v2 == e.v1)))
				return;
		}
		m_EdgeList.add(e);
	}
	
	public void clear()
	{
		m_EdgeList.clear();
		m_VertexList.clear();
	}
	
	public void loadScene(String fullPath)
	{
		clear();
		try {
			int linesToRead = 0;
			FileReader fr = new FileReader(fullPath);
			BufferedReader br = new BufferedReader(fr);
			String thisLine = br.readLine();
			if (null != thisLine)
			{
				linesToRead = Integer.parseInt(thisLine);
				for (; linesToRead > 0; linesToRead--)
				{
					if (null != thisLine)
					{
						thisLine = br.readLine();
						float x = Float.parseFloat(thisLine.split(" ")[0]);
						float y = Float.parseFloat(thisLine.split(" ")[1]);
						addVertex(new Vector2d(x, y));
					}
				}
				thisLine = br.readLine();
				if (null != thisLine)
				{
					linesToRead = Integer.parseInt(thisLine);
					for (; linesToRead > 0; linesToRead--)
					{
						if (null != thisLine)
						{
							thisLine = br.readLine();
							int u = Integer.parseInt(thisLine.split(" ")[0]);
							int v = Integer.parseInt(thisLine.split(" ")[1]);
							addEdge(new Edge(u, v));
						}
					}
				}
				br.close();
				fr.close();	
			}
        }
		catch (Exception e) {}
	}
	
}
