package computerGraphics01;

public class Vector2d {
	public final float Vx;
	public final float Vy;
	public final float Vz;
	
	public Vector2d(float x, float y)
	{
		Vx = x;
		Vy = y;
		Vz = 1;
	}
	
/*	public Vector(int r,  float deg)
	{
//TODO
		Vx = 0;
		Vy = 0;
		Vz = 0;
	}*/
	
	public Matrix toMatrix()
	{
		Matrix m = new Matrix(1,3);
		m.setValue(0, 0, Vx);
		m.setValue(1, 0, Vy);
		m.setValue(2, 0, Vz);
		
		return m;
	}
	
}
