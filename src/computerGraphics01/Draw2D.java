package computerGraphics01;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import computerGraphics01.World.Edge;

import java.util.ArrayList;

public class Draw2D {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Frame myFrame = new Frame("Exercise1");
		MyCanvas myCanvas = new MyCanvas(myFrame);
		myFrame.add(myCanvas);
		myFrame.addKeyListener(myCanvas);

		WindowAdapter myWindowAdapter = new WindowAdapter(){
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		};
		
		myFrame.addWindowListener(myWindowAdapter);
		myFrame.pack();
		myFrame.setVisible(true);
	}

}

class MyCanvas extends Canvas implements MouseListener,  MouseMotionListener, KeyListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyCanvas(Frame parent) {
		m_parent = parent;
		m_windowSizeY = 480;
		m_windowSizeX = 600;
		m_viewMatrix = Matrix.identMatrix(3);
		loadView("");
		setSize(m_windowSizeX, m_windowSizeY);
		addMouseListener(this);
		addMouseMotionListener(this);
		m_world = new World();
		m_world.loadScene("");
		addKeyListener(this);
		m_crntTransform = Matrix.identMatrix(3);
	}
	Point pStart, pEnd;
	boolean bFlag = false;
	private World m_world;
	private Matrix m_viewMatrix;
	private Matrix m_crntTransform;
	private int m_windowSizeY;
	private int m_windowSizeX;
	private int transformAction;
	private String m_lastViewFile;
	private Frame m_parent;
	
	public void paint(Graphics g) {
		g.drawLine(20, 20, 20, m_windowSizeY - 20);
		g.drawLine(20, 20, m_windowSizeX - 20, 20);
		g.drawLine(m_windowSizeX - 20,  m_windowSizeY - 20, 20, m_windowSizeY - 20);
		g.drawLine(m_windowSizeX - 20, m_windowSizeY - 20, m_windowSizeX - 20, 20);
		drawWorld(m_world, m_crntTransform, m_viewMatrix, g);
	}
	
	public void loadView(String fullPath)
	{
		m_lastViewFile = fullPath;
		int originX = 0;
		int originY = 0;
		float direction = 0;
		float sizeX = 1f;
		float sizeY = 0.5f;
		float resolX = 1;
		float resolY = 1;
		//set view params
		try {
			int linesToRead = 4;
			FileReader fr = new FileReader(fullPath);
			BufferedReader br = new BufferedReader(fr);
			for (; linesToRead > 0; linesToRead--)
			{
				String thisLine = br.readLine();
					if (null != thisLine)
					{
						String splitted[] = thisLine.split(" ");
						if (splitted[0].equalsIgnoreCase("Origin"))
						{
							originX = Integer.parseInt(splitted[1]);
							originY = Integer.parseInt(splitted[2]);
						}
						
						if (splitted[0].equalsIgnoreCase("Direction"))
						{
							direction = Float.parseFloat(splitted[1]);
						}
						
						if (splitted[0].equalsIgnoreCase("Size"))
						{
							sizeX = Float.parseFloat(splitted[1]);
							sizeY = Float.parseFloat(splitted[2]);
						}
						
						if (splitted[0].equalsIgnoreCase("Resolution"))
						{
							resolX = Float.parseFloat(splitted[1]);
							resolY = Float.parseFloat(splitted[2]);
						}
						
					}
			}
			
			br.close();
			fr.close();	
			}
        
		catch (Exception e) {}
		//build view matrix
		if (resolY != 1)
			m_windowSizeY = (int)resolY + 40;
		if (resolX != 1)
			m_windowSizeX = (int)resolX + 40;
		m_viewMatrix = Matrix.translate2d(20-originX, 20-m_windowSizeY-originY);
		m_viewMatrix = Matrix.rotateOrigin2d(m_windowSizeX/2, -m_windowSizeY/2, -direction).mult(m_viewMatrix);
		m_viewMatrix = Matrix.scaleOrigin2d(m_windowSizeX/2, -m_windowSizeY/2,sizeX/resolX, sizeY/resolY).mult(m_viewMatrix);
		m_viewMatrix = Matrix.scale2d(1, -1).mult(m_viewMatrix);
		setSize(m_windowSizeX, m_windowSizeY);
	}
	
	public void drawWorld(World w, Matrix crntTransform, Matrix totalTransform, Graphics g)
	{
		Matrix total = crntTransform.mult(totalTransform);
		List<Matrix> formedVecs = new ArrayList<Matrix>();
		for (int i = 0; i < w.m_VertexList.size(); i++)
		{
			Matrix formed = total.mult(w.m_VertexList.get(i).toMatrix());
			formedVecs.add(formed);
			formed.print();
		}
		
		for (int i = 0; i < w.m_EdgeList.size(); i++)
		{
			int u = w.m_EdgeList.get(i).v1;
			int v = w.m_EdgeList.get(i).v2;
					
			g.drawLine((int)formedVecs.get(u).m[0][0], (int)formedVecs.get(u).m[1][0]
					, (int)formedVecs.get(v).m[0][0], (int)formedVecs.get(v).m[1][0]);
		}
		
	}
	
	public void mouseClicked(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
		pStart = arg0.getPoint();
		transformAction = 0;
		if ((pStart.x < (m_windowSizeX / 3)) || pStart.x > (2 * m_windowSizeX / 3))
			transformAction += 1;
		if ((pStart.y < (m_windowSizeY / 3)) || pStart.y > (2 * m_windowSizeY / 3))
			transformAction += 1;
		
		
	}

	public void mouseReleased(MouseEvent arg0) {
		m_viewMatrix = m_crntTransform.mult(m_viewMatrix);
		m_crntTransform = Matrix.identMatrix(3);
		this.repaint();
		transformAction = -1;
	}

	public void mouseDragged(MouseEvent arg0) {
		pEnd = arg0.getPoint();
		switch(transformAction)
		{
		case 0: //middle screen : translate
			m_crntTransform = Matrix.translate2d(pEnd.x - pStart.x , pEnd.y - pStart.y);
			break;
		case 1: //middle+corner screen : scale
			int startDist = (int) Math.pow(Math.pow(m_windowSizeX/2 - pStart.x , 2) + Math.pow(m_windowSizeY/2 - pStart.y , 2), 0.5);
			int endDist = (int) Math.pow(Math.pow(m_windowSizeX/2 - pEnd.x , 2) + Math.pow(m_windowSizeY/2 - pEnd.y , 2), 0.5);
			//float scaleFactor =  (endDist - startDist) * 0.2f;
			//if (0 > scaleFactor)
			//	scaleFactor = -1 / scaleFactor;
			m_crntTransform = Matrix.scaleOrigin2d((float)m_windowSizeX/2, (float)m_windowSizeY/2, (float)Math.pow(1.1,  startDist - endDist), (float)Math.pow(1.1,  startDist - endDist));
			//m_crntTransform = Matrix.scaleOrigin2d((float)m_windowSizeX/2, (float)m_windowSizeY/2, scaleFactor, scaleFactor);
			break;
		case 2: //corner screen : rotate
			int xStart = pStart.x - m_windowSizeX/2;
			int yStart = pStart.y - m_windowSizeY/2;
			int xEnd = pEnd.x - m_windowSizeX/2;
			int yEnd = pEnd.y - m_windowSizeY/2;
			float angleStart = (float) Math.toDegrees(Math.atan2(yStart, xStart));
			float angleEnd = (float) Math.toDegrees(Math.atan2(yEnd , xEnd));
			m_crntTransform = Matrix.rotateOrigin2d((float)m_windowSizeX/2, (float)m_windowSizeY/2, angleStart -angleEnd);
			//m_crntTransform = Matrix.rotateOrigin2d((float)m_windowSizeX/2, (float)m_windowSizeY/2, (float)Math.pow(Math.pow(  pStart.x - pEnd.x, 2) + Math.pow(  pStart.y - pEnd.y, 2), 0.5));
			break;
		default:
			break;
		}
		bFlag = true;
		this.repaint();
	}

	public void mouseMoved(MouseEvent arg0) {}

	public void mouseEntered(MouseEvent arg0) {}

	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent e) {
		char key = e.getKeyChar();

	    switch(key)
	    {
	    case 'r':
	    case 'R':
	    	loadView(m_lastViewFile);
	    	this.repaint();
	    	break;
	    	
	    case 'l':
	    case 'L':
	    	FileDialog d = new FileDialog(m_parent);
	    	d.setVisible(true);
	    	String s = d.getFile();
	    	String directory = d.getDirectory();
	    	if (null != s)
	    	{
	    	String fileType = s.split("\\.")[1];
	    	if("viw".equalsIgnoreCase(fileType))
	    		loadView(directory + "\\" + s);
	    	else if("scn".equalsIgnoreCase(fileType))
	    		m_world.loadScene(directory + "\\" + s);
	    	else System.out.println("wrong file format");
	    	this.repaint();	
	    	}
	    	break;
	    	
	    case 'q':
	    case 'Q':
	    	System.exit(0);
	    	break;   	
	    }
	}

	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyReleased(KeyEvent e) {}

}